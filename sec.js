const puppeteer = require('puppeteer'); //Duh.
const easyscrape = require('easy-web-scraper'); //A quick node module I wrote to handle console.log and writing to a csv file.
const converter = require('json-2-csv'); // Handles conversion from returned JSON to CSV so it can be saved.
const fs = require('fs'); //Saves csv file to local drive.

const saveAs = 'csv/sec.csv'; //File path for outputted data.

function run () {
    return new Promise(async (resolve, reject) => {
            //This code clears the csv file so repeated running of this scraper doesn't just duplicate data.
            fs.truncate(saveAs, 0, function(err){
              console.log("File cleared.");
            });
            //This is boilerplate Puppeteer code that launches the browser. Settings are defined here.
            const browser = await puppeteer.launch({
              headless:true, // Makes scraper run headless or not.
              defaultViewport: null, // Makes sure the viewport matches (roughly) what it would be if you went there on your commputer (ie. full screen).
              slowMo:0 // Slows down Puppeteer so things don't break.
            });
            //Launch a new page.
            const page = await browser.newPage();
            //Navigate to the page to be scraped.
            const url = 'https://www.sec.gov/edgar/search/#/q=CEWS&dateRange=1y&category=custom&forms=10-Q%252C10-QT' //The URL to scrape.
            await page.goto(url, {
        		  waitUntil: 'networkidle0',
        		});

            allData = [];

            let items = await page.$$('.table > tbody > tr');
            let targets = await page.$$('a.preview-file');


            for (item in items) {

              let entry = {};
              let thing = item
              if (item < items.length)  {
                thing++
              }
              try {
                entry['name'] = await page.$eval('#hits > .table > tbody > tr:nth-child(' + thing + ")" , el => el.querySelector('td.entity-name').innerText);
                entry['filetype'] = await page.$eval('#hits > .table > tbody > tr:nth-child(' + thing + ")" , el => el.querySelector('td.filetype').innerText);
                entry['filed'] = await page.$eval('#hits > .table > tbody > tr:nth-child(' + thing + ")" , el => el.querySelector('td.filed').innerText);
                entry['reportingFor'] = await page.$eval('#hits > .table > tbody > tr:nth-child(' + thing + ")" , el => el.querySelector('td.enddate').innerText);
                entry['located'] = await page.$eval('#hits > .table > tbody > tr:nth-child(' + thing + ")" , el => el.querySelector('td.located').innerText);
                entry['incorporated'] = await page.$eval('#hits > .table > tbody > tr:nth-child(' + thing + ")" , el => el.querySelector('td.incorporated').innerText);
                entry['fileNo'] = await page.$eval('#hits > .table > tbody > tr:nth-child(' + thing + ")" , el => el.querySelector('td.file-num').innerText);

              } catch(e) {
                browser.close();
                return resolve(allData);
              }

              await targets[item].click()
              const frame = await page.frames().find(frame => frame.name() === 'ipreviewer');
              await frame.waitForSelector('.sect-efts-search-match-khjdickkwg', {visible:true}) //This class is the class of the highlighted text that the EDGAR text search creates.


              entry['text'] = await frame.evaluate(() => {
                return document.querySelector('.sect-efts-search-match-khjdickkwg').parentElement.innerText
              });

              await page.click('button.close')

              allData.push(entry)

            }


          })
        }
//This executes the above defined code.
run().then((response) => {
  easyscrape.logIt(response, saveAs);
}).catch(console.error);
